var express   = require('express');
var fs        = require("fs");
var path      = require("path");
var config    = require ('../config');
var mongoose  = require('mongoose');
var Image     = require('../imageSchema.js');
var router    = express.Router();


/* GET picture */
router.get('/', function(req, res, next) {
  // If feature flag is enabled, find image with the most views
  if (config.image.topviews == 1) {
    Image.findOne({}).sort({views: -1}).exec(function(err, topImage) {
      if (err) {
        console.log('Error was thrown...');
        throw (err);
      } else {
        console.log("Most views ID: " + topImage.id);
        req.topImage = topImage;
      }
    });
  }
  else {
    req.topImage = null;
  }
 
  // Find top based on frontpagelimit and render it
  Image.find({}).sort({date: -1}).limit(config.image.frontpagelimit).exec(function(err, images) { 
    if (err || !images.length) {
      res.status(404);
      res.render('error', {
        title : 'flackr',
        message : 'Object returned from database is empty.'
      });
    }
    else {
      res.render('index', {
        title : 'flackr',
        images : images,
        lastID : images[0].id,
        topView : req.topImage
      });
    }
  });
});

/* GET :id */
router.get('/:id', function(req, res, next) {
  // Find one image based on route
  Image.findOne({id: req.params.id}, function(err, oneImage) { 
    // Checks if findOne returns err, nothing or id is invalid
    if(err || oneImage == null || isNaN(req.params.id) == true) {
      // Return 404 with error page
      res.status(404);
      res.render('error', {
        title : 'flackr',
        message : 'Document not found.' 
      }); 
    } else {
      // Increment if page is visited
      oneImage.views++;
      oneImage.save();

      res.render('image', {
        title : 'flackr',
        image : oneImage
      });
    } 
  });
});

module.exports = router;

