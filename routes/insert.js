var express = require('express');
var mongoose = require('mongoose');
var Image = require('../imageSchema.js');
var Image = mongoose.model('Image');
var request = require('request');
var fs = require('fs');
var config = require ('../config');

var router = express.Router();

var placeholder = 'http://placehold.it/1280x720';

// Downloads the image we got, splitted out so this can be done async
function handlerImageDownload(response, localPath, callback) { 
  // Creates a stream to the path provided
  var writeStream = fs.createWriteStream(localPath);
  // Writing data
  response.pipe(writeStream);
  // When done writing data, check the size and send it back
  response.on('end', function() {
    fs.stat(localPath, function (err, stats) {
      size = stats['size'];
      callback(size);
    });
  });
};

// Query the database to find the ID for the new insert
function handlerDatabaseQuery(callback) {
  // Mongoose query
  Image.findOne().sort({'id': -1}).exec(function(err, query) {
    // If error from database, return with error
    if(err) {
      console.log('Error occured');
      return;
    } else if (query === null) { 
    // Special case; if database is empty we don't know the last insert ID
      var newId = 1;
      console.log('New ID: ' + newId);
      callback(newId);
    } else {
      // Error handling and special case, last is that we get an ID
      console.log('Last ID: ' + query.id);
      var newId = query.id+1;
      console.log('New ID: ' + newId);
      callback(newId)
    }
  });
}

/* GET New User  */
router.get('/newUser', function(req, res, next) {
  // Database handling
  // Query to find the last ID
    handlerDatabaseQuery(function(newId) {

    // Filepath concat
    var imagepath = config.image.folder + newId + '.jpg';

    // Getting the scheme ready from query string and ID
    var imageSchema = new Image({
      id : newId,
      title : req.query.title,
      publisher : req.query.publisher,
      views : 0,
      comments : [],
      date : Date.now()
    });

    // Does a request for an image to the URL specified
    request.get(req.query.image).on('response', function(response) {
      var contype = response.headers['content-type'];
      // Checking if we got an image back
      if (!contype || contype.indexOf('image/jpeg') !== 0) {
        res.writeHead(400);
        res.end('RESPONSE NOT AN IMAGE');
      } else if (contype === 'image/jpeg') {
        handlerImageDownload(response, imagepath, function(size) {
          // Verifying the datasize 
          if (size >= 10000) {
            // Save Mongoose schema to database with error handling
            imageSchema.save(function (err, saveImage) {
              if (err) {
               res.writeHead(500);
               res.end('ERROR WRITING TO DB');
              }
            });
          } else {
            res.writeHead(500);
            res.end('NOT ENOUGH DATA FOR IMAGE');
          }
        });
        // Everything went ok!
        res.writeHead(200);
        res.end('SAVED IMAGE, WROTE TO DATABASE');
      } else { // Everything else is responded with internal server error
        res.writeHead(500)
        res.end('INTERNAL SERVER ERROR'); 
      }
    });
  });
});


/* Get new comment */
router.get('/newComment', function(req, res, next) {
    var comment = { name: req.query.name, text: req.query.text, date: Date.now() };
      Image.findOneAndUpdate({'id' : req.query.id},
        {$push: {comments: comment}},
        {$safe: true, /*upsert: true*/},
          function(err, imageComment) {
            res.jsonp(imageComment);
          }
      );
});

module.exports = router;

