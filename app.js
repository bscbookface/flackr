/*   Flackr
     IMT3912 Bacheloroppgave IMT
*/

//  Mongoose module, configuration file and schema
var mongoose = require('mongoose');
var config = require('./config');
var Schema = mongoose.Schema;

// Database connection
connection = ('mongodb://' + (config.mongodb.username) + ':' + (config.mongodb.password) + '@' + (config.mongodb.ip) + '/' + (config.mongodb.name));
console.log(connection);

var options = {
  auth: {authdb: "admin"}
};

mongoose.connect(connection, options , function(err) {
  if (err) {
    console.log('Error connecting to database', err);
  } else {
    console.log('Connected to database');
  }
});

// Make a scheme based on template
var Image = require('./imageSchema.js');


// Requires from generator
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

// Define express instance
var app = express();

// Application endpoints
var routes = require('./routes/index');
var insert = require('./routes/insert');


// View engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// Uses
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico'))); not used
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(config.image.folder ));
app.disable('x-powered-by');

// npm start
app.listen(3000, function(err) {
  console.error('press CTRL+C to exit');
});

// Loading router modules in the application
app.use('/', routes);
app.use('/insert', insert);


// Catching 404 and forward to error handler 
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// Development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// Production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
