var config = {};

config.mongodb = {}; 
config.image = {};

config.mongodb.ip = '192.168.200.133';
config.mongodb.name = 'flackr';

config.mongodb.username = 'flackr';
config.mongodb.password = 'flackrPassword';

config.image.frontpagelimit = 100;

config.image.folder = '/data/imagestorage/'

config.image.topviews = 0;

module.exports = config;
