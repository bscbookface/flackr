# Welcome to Flackr! 
The #1 website for sharing your images!

This site is meant as an educational tool for learning to set up and manage 'big web' applications. The code is not meant to run in production. It is meant to let aspiring sysadmins experience several fault-scenarios so they can improve their skills. 

# Installing dependencies (assuming Ubuntu 14.04)

As a minimum requirement, Flackr needs a MongoDB database and an application server with Node JS package installed. In order to simplify the management, the use of a process manager named pm2 will also be installed. Eventually, several application servers will be load balanced and using GlusterFS to distribute the images between them.

All the instructions assume you are root on the machine in question.

## Application server
Installation and configuration of Flackr.

### Installing required packages

```
apt-get update
apt-get install nodejs-legacy npm git
```

### Flackr

Clone the Flackr repository using git package previously installed, and checkout the version you have to use. Currently there are three versions. In this example the repository is named 'flackr'. 

```
INSERT GIT SOMETHING HERE
```

Change directory to Flackr
```
cd flackr/
```

Create a folder to store the images. Make sure the correct user have ownership of the folder, especially if created with sudo. Same user running Flackr should have ownership of it. In this example we assume the owner will be username 'ubuntu' in group 'ubuntu'.
```
mkdir ~/images/
chown ubuntu:ubuntu ~/images
```

Copy the example configuration file. 

```
cp template_config.js config.js
```

Edit the config.js file and fill out the variables. The variable 'config.image.folder' is the one you created earlier.


### Installing and configuring process manager PM2
Flackr can be run without using a process manager using 
```
node app.js
```

By using something like this, the app can automatically restart if it crashes, insight to runtime performance and resource consumption. It is highly recommended! This will install and configure pm2 with logrotation.

```
npm install pm2 -g
pm2 install pm2-logrotate
pm2 startup ubuntu
pm2 logrotate -u ubuntu
```

To run Flackr with PM2

```
pm2 start app.js --name "Flackr"
```


## Database setup
Installation and configuration of MongoDB

Import the MongoDB official public GPG key, create a list file, update apt-get and then install the latest stable version of MongoDB.

```
1. sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927

2. echo "deb http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list

3. sudo apt-get update

4. sudo apt-get install -y mongodb-org
```

Now thats done, we have to configure the interface it will listen to.
```
vi /etc/mongod.conf/
```

Add/edit the following in /etc/mongod.conf

```
# network interfaces
net:
  port: 27017
  bindIp: 0.0.0.0
```

### User authentication
In order to create a user for Flackr to read from database do the following.

```
use admin
 db.getUsers()
[
    {
		"_id" : "admin.userAdmin",
		"user" : "userAdmin",
		"db" : "admin",
		"roles" : [
			{
				"role" : "userAdminAnyDatabase",
				"db" : "admin"
			}
		]
	},
	{
		"_id" : "admin.flackr",
		"user" : "flackr",
		"db" : "admin",
		"roles" : [
			{
				"role" : "readWrite",
				"db" : "flackr"
			}
		]
	}
]
```

Add/edit the following in /etc/mongod.conf
```
security:
  authorization: enabled
```

Then restart the MongoDB service

```
service mongod restart
```

In order to access MongoDB, you can use the following command
```
mongo -u "userAdmin" -p "userAdminPassword" --authenticationDatabase "admin"
```
