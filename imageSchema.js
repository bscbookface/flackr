var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

//Schema declarations for use with mongodb.

var Comment = new Schema ({
    name        : String,
    text        : String,
    date        : {type: Date, default: Date.now }
});

var Image = new Schema({
    id          : {type: Number, unique: true},
    title       : String,
    publisher   : String,
    views       : Number,
    comments    : [Comment],    // array of Comment schemas 
    date        : {type: Date, default: Date.now }
});


module.exports = mongoose.model('Image', Image);
